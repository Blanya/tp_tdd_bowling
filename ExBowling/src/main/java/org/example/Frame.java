package org.example;

import org.example.exceptions.NumberPinException;
import org.example.exceptions.StrikeException;
import org.example.exceptions.ThrowException;

public class Frame {
    private int count; //compteur de frame 1-9 => 2 lancers,frame 10 => 3 lancers
    private int nbPin; //nb de quilles restantes
    private int score; //score

    private int _fstThrow; //premier lancer du round
    private int _scdThrow; //second lancer du round
    private int _thdThrow; //3e lancer du round pour le round 10

    private boolean isStrike;
    private boolean isSpare;

    private Throw _aThrow;


    public Frame(Throw aThrow, int round) {
        nbPin = 10;
        _aThrow = aThrow;
        this.count = round;
    }

    public int getCount() {
        return count;
    }

    public int getNbPin() {
        return nbPin;
    }

    public int getScore() {
        return score;
    }

    public boolean isStrike() {
        return isStrike;
    }

    public boolean isSpare() {
        return isSpare;
    }

    public int countScore() throws Exception {
        _fstThrow = _aThrow.getPins(10);

        if (_fstThrow == 10)
        {
            isStrike = true;
        }

        if(count == 10 && isStrike){
            _scdThrow = _aThrow.getPins(10);

            if(_scdThrow == 10)
                _thdThrow = _aThrow.getPins(10);
            else
                _thdThrow = _aThrow.getPins(nbPin-_scdThrow);
            
            return score = 10 + _scdThrow + _thdThrow;
        }

//        else if(isStrike)
//                throw new StrikeException();
            else {

                _scdThrow = _aThrow.getPins(nbPin - _fstThrow);

                if (_fstThrow + _scdThrow == 10 && _fstThrow != 10) {
                    isSpare = true;
                }
                if(count == 10 && isSpare )
                {
                    _thdThrow = _aThrow.getPins(10);
                    return score = 10 + _thdThrow * 2;
                }
            }

            score = _fstThrow + _scdThrow;

        if(score > 10)
            throw new ThrowException();
        return score;
    }

}
