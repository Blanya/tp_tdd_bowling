package org.example;

import org.example.exceptions.NumberPinException;
import org.example.exceptions.StrikeException;
import org.example.exceptions.ThrowException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.misusing.PotentialStubbingProblem;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FrameTest {

    private Frame frame;

    @Mock
    private Throw aThrow;

    @BeforeEach
    void setUp() throws Exception {
        frame = new Frame(aThrow, 1);
        Mockito.when(aThrow.getPins(10)).thenReturn(7);
    }

    @Test
    void testCountScoreShouldBe7IfFirstThrow() throws Exception {
        Assertions.assertEquals(7, frame.countScore());
    }

    @Test
    void testCountScoreShouldBe9IfSecondThrowAndPinsAre2 () throws Exception {
        //A
        Mockito.when(aThrow.getPins(3)).thenReturn(2); //Second lancer
        Assertions.assertEquals(9, frame.countScore());
    }

    @Test
    void testCountScoreShouldNotBeGT10Pins() throws Exception {
        //A
        Mockito.when(aThrow.getPins(4)).thenReturn(4); //Second lancer
        Assertions.assertThrowsExactly(PotentialStubbingProblem.class, () -> frame.countScore());
    }

    @Test
    void testCountScoreShouldBe7IfFirstThrowGive7AndSecondThrowFall() throws Exception {
        Mockito.when(aThrow.getPins(3)).thenReturn(0); //Second lancer
        Assertions.assertEquals(7, frame.countScore());
    }

    @Test
    void testIsStrikeShouldBeTrueIfFirstThrow10Pins() throws Exception {
        //A
        frame = new Frame(aThrow, 1);
        Mockito.when(aThrow.getPins(10)).thenReturn(10);
        frame.countScore();

        Assertions.assertTrue(frame.isStrike());
    }

    @Test
    void testIsSpareShouldBeTrueIfBothThrows10Pins() throws Exception {
        //A
        Mockito.when(aThrow.getPins(3)).thenReturn(3);
        frame.countScore();

        Assertions.assertTrue(frame.isSpare());
    }

    @Test
    void testScoreIs10IfFirstRoundIsStrike() throws Exception {
        frame = new Frame(aThrow, 1);
        Mockito.when(aThrow.getPins(10)).thenReturn(10);
        Mockito.when(aThrow.getPins(10)).thenReturn(3); //Second lancer après strike

        Assertions.assertThrowsExactly(StrikeException.class, ()-> frame.countScore());
    }

    @Test
    void testRound10ScdThrowIfStrikeFstThrow() throws Exception {
        frame = new Frame(aThrow, 10);
        Mockito.when(aThrow.getPins(10)).thenReturn(10);
        frame.countScore();
        Mockito.when(aThrow.getPins(10)).thenReturn(3);

        Assertions.assertEquals(13,frame.countScore());
    }

    @Test
    void testRound10ScdAndThdThrowIfStrikeFstThrow() throws Exception {
        frame = new Frame(aThrow, 10);
        Mockito.when(aThrow.getPins(10)).thenReturn(10);
        frame.countScore();
        Mockito.when(aThrow.getPins(10)).thenReturn(3);
        Mockito.when(aThrow.getPins(7)).thenReturn(5);

        Assertions.assertEquals(18,frame.countScore());
    }

    @Test
    void testRound10WithTwoThrowsShouldBeException() throws Exception {
        frame = new Frame(aThrow, 10);
        Mockito.when(aThrow.getPins(10)).thenReturn(2);
        Mockito.when(aThrow.getPins(8)).thenReturn(6);
        Mockito.when(aThrow.getPins(10)).thenReturn(5);

        Assertions.assertThrowsExactly(PotentialStubbingProblem.class, () -> frame.countScore());
    }

    @Test
    void testRound10With3Strike() throws Exception {
        frame = new Frame(aThrow, 10);
        Mockito.when(aThrow.getPins(10)).thenReturn(10);
        frame.countScore();
        Mockito.when(aThrow.getPins(10)).thenReturn(10);
        Mockito.when(aThrow.getPins(10)).thenReturn(10);

        Assertions.assertEquals(30,frame.countScore());
    }

    @Test
    void testRound10ThdThrowIfSpare() throws Exception {
        frame = new Frame(aThrow, 10);
        Mockito.when(aThrow.getPins(10)).thenReturn(7);
        Mockito.when(aThrow.getPins(3)).thenReturn(3);
        frame.countScore();
        Mockito.when(aThrow.getPins(10)).thenReturn(5);

        Assertions.assertEquals(20,frame.countScore());
    }
}
